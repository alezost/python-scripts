#!/usr/bin/env python3

import random

def firstNum (variantsAmount=365):
    "Return the number of the first duplicate."
    items = []
    while True:
        item = random.randrange(0, variantsAmount)
        if item in items:
            break
        else:
            items.append(item)
    return len(items)+1

res = 0
variantsAmount = int(input("Enter the number of variants: "))
iterAmount     = int(input("Enter the number of iterations: "))
print()
for i in range(1, iterAmount+1):
    amount = firstNum(variantsAmount)
    #print("iteration %d:\t%d" % (i, amount))
    res += amount
res /= i
print("average:", res)
