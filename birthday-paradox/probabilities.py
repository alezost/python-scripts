#!/usr/bin/env python3

def p (variants, num):
    "Return the probability of reaching the number NUM."
    if num == 1:
        res = 1
    else:
        res = (variants - num + 1) / variants * p(variants, num-1)
    print("%3d  %.8f  %.8f" % (num, round(1-res, 8), round(res, 8)))
    return res

p(365, 100)
