#!/usr/bin/env python2

# Copyright (C) 2012 Alex Kost

# Created: 11 Nov 2012

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Commentary:

# This script prints torrent name of the specified torrent file.  It
# relies on the python bindings provided by "libtorrent-rasterbar"
# library: <http://www.rasterbar.com/products/libtorrent/>.

import sys
import libtorrent

if (len(sys.argv) < 2):
    print "Missing param: torrent filename"
    sys.exit()

torrent = sys.argv[1]
info = libtorrent.torrent_info(torrent)
print info.name()
