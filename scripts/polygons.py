#!/usr/bin/env python3

# Copyright (C) 2012 Alex Kost

# Created: 11 Dec 2012

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from turtle import *

def paint_polygon (pen, side_amount, side_len):
    "Paint a polygon with N sides with a turtle PEN."
    angle = 360./side_amount
    #pen.hideturtle()
    pen.color("black", "blue")
    pen.begin_fill()
    pen.left((180 - angle)/2)
    for i in range(side_amount):
        pen.forward(side_len)
        pen.left(-angle)
    pen.end_fill()

def paint_star (pen, angle, side_len):
    """Paint a star by painting lines with length SIDE_LEN
    and angle ANGLE."""
    pen.color('red', 'yellow')
    pen.begin_fill()
    for i in range(int(360/angle)):
        pen.forward(side_len)
        pen.left(180 - angle)
    pen.end_fill()
    #done()

t = Pen()
t.shape("turtle")
t.speed(5)
t.pu()
t.goto(-200,0)
t.pd()
paint_polygon(t, 36, 30)
t.seth(0)
paint_star(t, 10, 300)
while True:
    l = input("Enter 'p' for polygon, 's' for star, 'e' for exit: ")
    t.goto(-200, 0)
    t.seth(0)
    t.clear()
    if l == 'p':
        side_amount = int(input("Enter the number of sides: "))
        side_len    = int(input("Enter the length of a side: "))
        paint_polygon(t, side_amount, side_len)
    elif l == 's':
        angle       = int(input("Enter the angle: "))
        side_len    = int(input("Enter the length of a side: "))
        paint_star(t, angle, side_len)
    elif l == 'e':
        break
    else:
        print("Wrong letter.")
#mainloop()
