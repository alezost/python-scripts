#!/usr/bin/env python3

# Copyright (C) 2012 Alex Kost

# Created: 23 Dec 2012

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Print random text to stdout by characters."""

import os
import sys
import random
from time import sleep

# WORDS_FILE = os.path.dirname(__file__) + '/words'
WORDS_FILE = '/usr/share/dict/words'

def load_words (filename):
    """Return a list of words from a file FILENAME.
    File should have one word in one line."""
    words_file = open(filename, 'r')
    wordlist = [word.strip() for word in words_file]
    words_file.close()
    return wordlist

def random_word (wordlist):
    """Return a randomly selected word from a WORDLIST."""
    #return wordlist[random.randrange(0, len(wordlist))]
    return random.choice(wordlist)

def random_element (dct):
    """Return a randomly selected key from a dictionary DCT.
    Values of DCT should be probabilities of keys.
    The sum of probabilities must be equal 1."""
    elements = []
    prob     = 0
    for key in dct:
        prob += dct[key]
        elements.append((key, prob))
    prob = random.random()
    #print("> ", elements)
    for elem in elements:
        if prob <= elem[1]:
            res = elem[0]
            break
    return res

def print_by_chars (text, delay = (0.05, 0.3)):
    """Print a text TEXT by characters with a delay after each of those.
    Delay is defined randomly from a tuple (min_delay, max_delay)."""
    for char in text:
        print(char, end='')
        sys.stdout.flush()
        sleep(random.uniform(delay[0], delay[1]))

def generate_sentence (wordlist,
                       words_amounts           = (2, 14),
                       punctuation_frequencies = (2, 7),
                       punctuation_chars       = {',': .6, ':': .15, ' -': .15, ';': .1},
                       end_chars               = {'.': .73, '?': .15, '!': .1, '...': .02},
                       new_line_probabilities  = (.1, .5)):
    """Return a sentence, randomly created with words from WORDLIST.
words_amounts - amount of words in a sentence;
punctuation_frequencies - amount of words between 2 punctuation chars;
new_line_probabilities - the probability of '\n' after the sentence.
punctuation_chars and end_chars are dictionaries {character: probability}:
the sum of probabilities must be equal 1.
words_amounts, punctuation_frequencies and new_line_probabilities are tuples
(min_value, max_value): values are randomly generated from this range.
"""
    assert words_amounts[1]>=words_amounts[0]
    assert punctuation_frequencies[1]>=punctuation_frequencies[0]
    assert new_line_probabilities[1]>=new_line_probabilities[0]

    words_amount = random.randrange(words_amounts[0], words_amounts[1]+1)
    words = [random_word(wordlist) for i in range(words_amount)]

    # punctuation making:
    punctuation_num = random.randrange(0, punctuation_frequencies[1])
    word_num = 0
    for i in range(len(words)-1):
        word_num += 1
        if punctuation_num <= word_num:
            words[i] += random_element(punctuation_chars)
            punctuation_num = random.randrange(punctuation_frequencies[0], punctuation_frequencies[1])
            word_num = 0

    sentence = ' '.join(words).capitalize()
    new_line_probability = random.uniform(new_line_probabilities[0], new_line_probabilities[1])
    sentence += random_element(end_chars) + \
                random_element({'\n': new_line_probability, ' ': 1-new_line_probability})
    return sentence

def print_random_text (sentences_amount = None):
    """Print a text of random sentences by characters.
    If SENTENCES_AMOUNT is not specified, do it infinitely."""
    i = 0
    wordlist = load_words(WORDS_FILE)
    while sentences_amount == None or i < sentences_amount:
        print_by_chars(generate_sentence(wordlist))
        i += 1
    print()

def main ():
    print_random_text()

if __name__ == '__main__':
    main()
