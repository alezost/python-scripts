#!/usr/bin/env python3

# Copyright (C) 2012 Alex Kost

# Created: 12 Dec 2012

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Paint patterns of circles using "turtle" module"""

import turtle

def circle_pattern (t, groups_amount, circles_amount, min_radius = 10, step = 10):
    """Paint a pattern of circles.
    T              - turtle;
    GROUPS_AMOUNT  - amount of groups of circles;
    CIRCLES_AMOUNT - amount of circles in a group;
    MIN_RADIUS     - radius of a minimal circle;
    STEP           - a step of radius changes in a group of circles."""
    t.begin_fill()
    for r in range(min_radius, min_radius + step*circles_amount, step):
        for i in range(groups_amount):
            t.circle(r)
            t.left(360/groups_amount)
    t.end_fill()

def example (t):
    "Example of painting a circle pattern with a turtle T."
    #t.shape("turtle")
    t.color("red", "yellow")
    circle_pattern(t, 7, 8, 14, 17)

def main ():
    t = turtle.Pen()
    t.hideturtle()
    t.speed(0)
    example(t)
    input("Enter any key to continue: ")
    t.color("black", "lightgreen")
    turtle.bgcolor('lightblue')
    while True:
        l = input("Enter 'n' for new pattern, any other key for exit: ")
        #t.seth(0)
        t.clear()
        if l == 'n':
            groups_amount  = int(input("Enter the number of groups of circles: "))
            circles_amount = int(input("Enter the number of circles in a group: "))
            circle_pattern(t, groups_amount, circles_amount)
        else:
            break
        #turtle.mainloop()

if __name__ == '__main__':
    main()
