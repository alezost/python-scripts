#!/usr/bin/env python3

# Copyright (C) 2013 Alex Kost

# Created: 2 Apr 2013

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import random
import argparse

def parse_args():
    "Parse current command-line arguments."
    parser = argparse.ArgumentParser \
             (description='Pseudo encrypting/decrypting a file.',
              epilog="By default a file will be encrypted, unless '-d' key is specified.")
    parser.add_argument('filepath', help='name of a de/en-crypting file')
    parser.add_argument('-m', '--magic-numbers', dest='mn', default='1,2',
                        help='2 comma-separated numbers for de/en-cryption')
    parser.add_argument('-o', '--output', default=None,
                        help='name of an output file')
    parser.add_argument('-f', '--force', action='store_const',
                        default=False, const=True,
                        help='when specified, overwrite output file if it exists')
    parser.add_argument('-d', '--decrypt', dest='crypt', action='store_const',
                        default=encrypt, const=decrypt,
                        help='decrypt specified file')
    return parser.parse_args()

def encrypt(mn, inpath, outpath = None, force = False):
    '''
    Create encrypted file OUTPATH from file INPATH.
    If OUTPATH is not specified, it will be generated from INPATH.
    If OUTPATH exists, it will be overwritten only if FORCE is True.
    inpath, outpath - strings (path to files);
    mn - tuple of 2 magic numbers for encryption;
    force - bool.
    '''
    inpath, outpath = check_paths(inpath, outpath, force)
    infile = open(inpath, 'rb')
    outfile = open(outpath, 'wb')

    outfile.write(random_bytes(mn[0]))
    read_bytes = infile.read(2)
    while read_bytes:   # read until EOF
        for i in (0,1):
            if len(read_bytes) > i:
                # byteorder ('little' or 'big') doesn't matter for a single byte
                outfile.write((shift_val(read_bytes[i], mn[i])).to_bytes(1, 'little'))
        read_bytes = infile.read(2)
    outfile.write(random_bytes(mn[1]))
    infile.close()
    outfile.close()

def decrypt(mn, inpath, outpath = None, force = False):
    '''
    Create decrypted file OUTPATH from file INPATH.
    If OUTPATH is not specified it will be generated from INPATH.
    If OUTPATH exists it will be overwritten only if FORCE is True.
    inpath, outpath - strings (path to files);
    mn - tuple of 2 magic numbers for decryption;
    force - bool.
    '''
    inpath, outpath = check_paths(inpath, outpath, force)
    infile = open(inpath, 'rb')
    outfile = open(outpath, 'wb')

    infile_size = os.stat(inpath).st_size
    infile.read(mn[0])
    read_bytes = infile.read(1)
    eof = False
    while not eof:
        for i in (0,1):
            if infile.tell() > infile_size - mn[1]:   # read until mn[1]
                eof = True
                break
            else:
                outfile.write((shift_val(read_bytes[0], -mn[i])).to_bytes(1, 'little'))
            read_bytes = infile.read(1)
    infile.close()
    outfile.close()

def check_paths(inpath, outpath = None, force = False):
    '''
    Check and create if needed file paths INPATH and OUTPATH:
    If OUTPATH is not specified it will be generated from INPATH.
    If OUTPATH exists the script will be continued only if FORCE is True.
    Return a tuple of checked (INPATH, OUTPATH).
    inpath, outpath - strings (path to files).
    force - bool.
    '''
    if not os.path.isfile(inpath):
        print('"%s" is not a regular file.' % inpath)
        exit()

    if outpath == None:
        import tempfile
        inpath = os.path.abspath(inpath)
        infile_name = os.path.basename(inpath)
        infile_dir  = os.path.dirname(inpath)
        outpath = tempfile.mkstemp(prefix = infile_name + '_', dir = infile_dir)[1]
    else:
        if os.path.exists(outpath):
            print('File "%s" exists.' % outpath, end=' ')
            if os.path.isfile(outpath) and force:
                print('It will be overwritten.')
            else:
                print("Use 'force' to overwrite it (perhaps it cannot be overwritten).")
                exit()
    return (inpath, outpath)

def shift_val(val, shift, bot = 0, top = 0xff):
    '''
    Shift the value VAL by SHIFT and return the result value.
    If the new value exceeds the BOT and TOP limits (0..255 by default),
    continue counting from zero.
    bot, top - integers;
    val, shift - integers in bottom and top limits (shift can be negative).
    '''
    assert (bot <= val <= top) and (bot <= abs(shift) <= top)
    return (val + shift) % (top - bot + 1)

def random_bytes(count):
    "Return a list of COUNT amount of random bytes."
    randlist = []
    for i in range(count):
        randlist.append(random.randint(0, 255))
    return bytearray(randlist)

def main():
    settings = parse_args()
    # Convert string to tuple:
    mn = tuple(map(int, (settings.mn).split(',')))
    settings.crypt(mn, settings.filepath, settings.output, settings.force)

if __name__ == '__main__':
    main()
