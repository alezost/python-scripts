#!/usr/bin/env python3

# Copyright (C) 2012 Alex Kost

# Created: 10 Dec 2012

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import turtle

def paint_curve (trt, length, min_length = 4):
    """Paint a Koch curve on a line LENGTH with a turtle TRT."""
    if length <= min_length:
        trt.forward(length)
    else:
        paint_curve(trt, length/3., min_length)
        trt.left(60)
        paint_curve(trt, length/3., min_length)
        trt.left(-120)
        paint_curve(trt, length/3., min_length)
        trt.left(60)
        paint_curve(trt, length/3., min_length)

def paint_hexagon (trt, length, min_length = 4):
    """Paint a hexagon of Koch curves on a line LENGTH with a turtle TRT."""
    if length <= min_length:
        trt.forward(length)
    else:
        magic_angle = 0
        for i in range(12):
            paint_curve(trt, length/3., min_length)
            magic_angle = 0 if magic_angle else 180
            trt.left(magic_angle - 120)

def main ():
    turtle.Screen()
    turtle.delay(1)
    #turtle.bgcolor("orange")
    trt = turtle.Turtle()
    trt.hideturtle()
    trt.speed(0)
    trt.penup()
    trt.goto(-200,100)
    trt.pendown()
    #paint_curve(trt, 400)
    # for i in range(3):
    #         paint_curve(t, 400)
    #         t.left(-120)
    trt.color("#4FB8E5")
    trt.begin_fill()
    paint_hexagon(trt, 400, 10)
    trt.end_fill()
    turtle.mainloop()

if __name__ == '__main__':
    main()
