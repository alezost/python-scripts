#!/usr/bin/env python3

# Copyright (C) 2013 Alex Kost

# Created: 9 Jan 2013

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Module provides 2 functions (to_roman, to_arabic) to convert
Roman numerals to decimal numbers and vice versa.
Only classical form of Roman is used : "XCIX" is good, "IC" is bad.
"""

import math
import re

ASSOCIATIONS       = (('I', 1),
                      ('V', 5),
                      ('X', 10),
                      ('L', 50),
                      ('C', 100),
                      ('D', 500),
                      ('M', 1000))
EXTRA_ASSOCIATIONS = (('IV', 4),
                      ('IX', 9),
                      ('XL', 40),
                      ('XC', 90),
                      ('CD', 400),
                      ('CM', 900))

def to_roman(arabic):
    """Return a roman number from the ARABIC one.
    arabic - int<4000."""
    assert(type(arabic) == int)
    if (0 < arabic < 4000):
        # Make a full associations list, ordered by decimals (descending).
        alist = sorted(ASSOCIATIONS + EXTRA_ASSOCIATIONS,
                       key = lambda t: t[1], reverse = True)
        roman = ''
        for assoc in alist:
            num_symbols = math.floor(arabic / assoc[1])
            arabic = arabic % assoc[1]
            roman += assoc[0] * num_symbols
            if arabic == 0: break
    else:
        print('"%d" is a wrong number (should be positive and < 4000).' % arabic)
        roman = None
    return roman

def to_arabic(roman):
    """Return an arabic number from the ROMAN one.
    roman - str."""
    assert(type(roman) == str)
    roman = roman.upper()
    if re.match('M{0,3}(D?C{0,3}|C[DM])(L?X{0,3}|X[LC])(V?I{0,3}|I[VX])$', roman):
        adict = {}
        for assoc in ASSOCIATIONS:
            adict[assoc[0]] = assoc[1]
        arabic = 0
        for i in range(len(roman) - 1):
            cur = adict[roman[i]]
            if cur < adict[roman[i+1]]:
                cur = -cur
            arabic += cur
        arabic += adict[roman[-1]]
    else:
        print('"%s" is a wrong Roman number' % roman)
        arabic = None
    return arabic

def main():
    answer = ''
    while answer != 'q':
        answer = input('Print "a"/"r" to convert arabic/roman number or "q" to quit: ')
        if answer == 'a':
            arabic = int(input("Print an arabic number: "))
            roman  = to_roman(arabic)
            if roman: print(roman)
        if answer == 'r':
            roman  = input("Print a roman number: ")
            arabic = to_arabic(roman)
            if arabic: print(arabic)

if __name__ == '__main__':
    main()
